from setuptools import setup

setup(
    name='bigbrother',
    version='0.1',
    packages=['bigbrother'],
    zip_safe=False,
    author='Eadan Fahey',
    author_email='eadanfahey@protonmail.com',
    description='A library for logging error / info messages to a file and sentry'
)
