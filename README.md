# bigbrother

A library for logging error / info messages. Messages can be logged to one or
more of the following:

  1. A file, specified using the `filename` parameter.
  2. An output stream such as `sys.stdout`, specified using the `stream` parameter.
  3. Sentry, whose DSN is specified using the `sentry_dsn` parameter.
  
## Installation

`pip install --upgrade git+ssh://git@bitbucket.org/foodmarble/bigbrother.git`

You need to add your SSH public key for bitbucket first.

## Example usage

```python
import bigbrother
import sys

try:
    1 / 0
except ZeroDivisionError as e:
    logger = bigbrother.logger(filename='/tmp/my_logs.jsonl',
                               stream=sys.stdout,
                               project='Test bigbrother')

    logger.info('This is an INFO message')
    logger.debug('This is a DEBUG message')
    logger.warning('This is a WARNING message')
    logger.critical('This is a CRITICAL message', exc_info=True)
```

Only `critical` errors are sent to Sentry. However, if a file or a stream is
given, all log levels will be written there.

```bash
tail -n4 /tmp/my_logs.jsonl | jq '.'
``` 

Example output:
```json
{
  "project": "Test bigbrother",
  "pathname": "test2.py",
  "level": "INFO",
  "time": "2018-02-08 16:28:02",
  "message": "This is an INFO message",
  "lineno": 11,
  "args": [],
  "exc_info": null
}
{
  "project": "Test bigbrother",
  "pathname": "test2.py",
  "level": "DEBUG",
  "time": "2018-02-08 16:28:02",
  "message": "This is a DEBUG message",
  "lineno": 12,
  "args": [],
  "exc_info": null
}
{
  "project": "Test bigbrother",
  "pathname": "test2.py",
  "level": "WARNING",
  "time": "2018-02-08 16:28:02",
  "message": "This is a WARNING message",
  "lineno": 13,
  "args": [],
  "exc_info": null
}
{
  "project": "Test bigbrother",
  "pathname": "test2.py",
  "level": "CRITICAL",
  "time": "2018-02-08 16:28:02",
  "message": "This is a CRITICAL message",
  "lineno": 14,
  "args": [],
  "exc_info": [
    "ZeroDivisionError",
    "division by zero",
    [
      "  File \"test2.py\", line 5, in <module>\n    1 / 0\n"
    ]
  ]
}
```
