'''
A library for logging error / info messages to a file and sentry.
'''
# -*- coding utf-8 -*-
import json
import logging
import logging.config
import datetime as dt
import traceback


class JsonFormatter(logging.Formatter):

    def __init__(self):
        super(JsonFormatter, self).__init__()

    def format(self, record):
        now = dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        exc = record.exc_info
        if exc is not None:
            new_exc = list(exc)
            new_exc[0] = exc[0].__name__
            new_exc[1] = str(exc[1])
            new_exc[2] = traceback.format_tb(exc[2])
            exc = tuple(new_exc)

        msg = {
            'project': record.project,
            'pathname': record.pathname,
            'level': record.levelname,
            'time': now,
            'message': record.msg,
            'lineno': record.lineno,
            'args': record.args,
            'exc_info': exc
        }
        return json.dumps(msg)


def logger(project=None, filename=None, sentry_dsn=None, stream=None):

    handlers = {}

    if sentry_dsn is not None:
        handlers['sentry'] = {
            'level': 'CRITICAL',
            'class': 'raven.handlers.logging.SentryHandler',
            'dsn': sentry_dsn
        }

    dict_log_config = {
        'version': 1,
        'handlers': handlers,
        'loggers': {
            'bigbrother': {
                'handlers': list(handlers.keys()),
                'level': 'DEBUG',
            },
        }
    }

    logging.config.dictConfig(dict_log_config)
    lg = logging.getLogger('bigbrother')

    if filename is not None:
        file_handler = logging.FileHandler(filename)
        file_handler.setFormatter(JsonFormatter())
        file_handler.setLevel('DEBUG')
        lg.addHandler(file_handler)

    if stream is not None:
        stream_handler = logging.StreamHandler(stream)
        stream_handler.setFormatter(logging.Formatter())
        stream_handler.setLevel('DEBUG')
        lg.addHandler(stream_handler)

    return logging.LoggerAdapter(lg, extra={"project": project}) 

